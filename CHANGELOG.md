## 0.0.6-alpha (2022-10-16)

No changes.

## 0.0.8-beta (2022-12-20)

* FBB-18 disable XP calculation. Revert to simple data field.
* FBB-17 When creating a weapon, there is a field for size. C&C does not have size as a weapon attribute. Dropped from item-detail-sheet
* FBB-16 Fixed hover over AC with Helmet misspelled.
* Swapped the Author to TLG for branding
* Embedded the TLG icon pack for global use
* Established a font folder for future use
* Corrected how item AC is displayed

## 0.0.9-beta (2023-2-24)

* [UPDATE] Spanish language (i18n) translations - [Elfonochasis](https://gitlab.com/troll-lord/foundry-vtt/ruleset/castles-and-crusades/-/commit/ee03b5a57ea5269ad8adfe2bcf3ed20750ad0719)
* [UPDATE] Updated Copyright notice (TLG) - [42datasquirrels](https://gitlab.com/42datasquirrels)
* [FIX] Cleaned up Foundry V10 warning messages by changing .data. references to .system. references - [pwatson100](https://gitlab.com/pwatson100)
* [UPDATE] All TinyMCE fields are automatically enriched (able to drop links to other items etc.) - [pwatson100](https://gitlab.com/pwatson100)
* [FIX] Issue #47: Creating a Spell as an Item and dragging it onto an actor sheet causes errors and breaks the actor sheet - [pwatson100](https://gitlab.com/pwatson100) 

