# Castles & Crusades: The Role Playing Game

[![Latest Release](https://gitlab.com/troll-lord/foundry-vtt/ruleset/castles-and-crusades/-/badges/release.svg)](https://gitlab.com/troll-lord/foundry-vtt/ruleset/castles-and-crusades/-/releases)
[![pipeline status](https://gitlab.com/troll-lord/foundry-vtt/ruleset/castles-and-crusades/badges/main/pipeline.svg)](https://gitlab.com/troll-lord/foundry-vtt/ruleset/castles-and-crusades/-/commits/main)  
The **Official** game system for playing [Castles & Crusades: The Role Playing Game](https://trolllord.com/castles-crusades/) on FoundryVTT.

Castles & Crusades: The Roleplaying Game, is an easy to learn, fast paced fantasy role playing game suitable for novice and veteran gamers of all ages. With Castles & Crusades, these imaginings come to life as you play the role of a hero seeking adventure in a fantastic world populated by mythic creatures and legendary beasts. Or, as the Castle Keeper, you can design the worlds and stories that make up the game, guiding friends and fellow gamers through epic adventures in wondrous settings of your own making. **Join Us at the Table!**

![Banner](https://raw.githubusercontent.com/42datasquirrels/castles_crusades/main/readme_banner.webp)

## Installation

1. Inside Foundry's Configuration and Setup screen, go to **Game Systems**
2. Click "Install System"
3. In the Manifest URL field, found at the bottowm, paste: `https://gitlab.com/troll-lord/foundry-vtt/ruleset/castles-and-crusades/-/raw/main/system.json`

## Official Modules

The following **official** modules are available:

- [Castles & Crusades Quickstart Guide](https://gitlab.com/troll-lord/foundry-vtt/modules/castles-crusades-quickstart/-/raw/main/module.json)

## Features

![Character Sheet](https://raw.githubusercontent.com/42datasquirrels/castles_crusades/main/readme_charactersheet_example.webp)

First when using the Character sheets think **Digital Paper** Many of the fields in the sheet do not have auto caculations enabled, Yet!
- Character sheets for PCs
- Roll Ability Check and Saves
- Can create Class Abilitys, Weapons, Gear, Armor and drag-n-drop onto character sheets.
- Can click on weapons to roll with bonus modifiers
- Track weight carried in the inventory tab

## How-Tos
Coming soon!

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## License

All software components are licensed under the MIT license - see _LICENSE.txt_ for details.

### Copyright Notices

* Open Game License v 1.0 Copyright 2000, Wizards of the Coast, Inc.
* Castles & Crusades: Players Handbook, Copyright 2022, Troll Lord Games; Authors Davis Chenault and Mac Golden.
* Castles & Crusades: Monsters Product Support, Copyright 2022, Troll Lord Games.
* The Basic Fantasy Field Guide Copyright © 2010 Chris Gonnerman and Contributors.
* Basic Fantasy Role-Playing Game Copyright © 2006-2015 Chris Gonnerman.
* Boilerplate System Copyright © 2020 Asacolips Projects / Foundry Mods.
* Basic Fantasy RPG for FoundryVTT © 2022 Steve Simenic.
